// Copyright © 2022 Daniel Quack - All Rights Reserved
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY. without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see<http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Collections.ObjectModel;
using ClubCalc.Models;

namespace ClubCalc.ViewModels;

public class CalculatorViewModel : ViewModelBase, ICalculatorViewModel
{
  public CalculatorViewModel()
  {
    People = new ObservableCollection<Person>(GenerateMockPeopleTable());
    VersionInfo = VersionInformation.VersionStringLong;
  }

  public ObservableCollection<Person> People { get; set; }

  public string VersionInfo { get; }

  private IEnumerable<Person> GenerateMockPeopleTable()
  {
    var defaultPeople = new List<Person>
    {
      new()
      {
        FirstName = "Pat",
        LastName = "Patterson",
        EmployeeNumber = 1010,
        DepartmentNumber = 100,
        DeskLocation = "B3F3R5T7"
      },
      new()
      {
        FirstName = "Jean",
        LastName = "Jones",
        EmployeeNumber = 973,
        DepartmentNumber = 200,
        DeskLocation = "B1F1R2T3"
      },
      new()
      {
        FirstName = "Terry",
        LastName = "Tompson",
        EmployeeNumber = 300,
        DepartmentNumber = 100,
        DeskLocation = "B3F2R10T1"
      }
    };
    return defaultPeople;
  }
}