// Copyright © 2022 Daniel Quack - All Rights Reserved
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY. without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Diagnostics;
using System.IO;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Markup.Xaml;
using ClubCalc.ViewModels;

namespace ClubCalc.Views;

public partial class AboutWindow : Window
{
  private readonly string projectUrl = "https://gitlab.com/quacky/clubcalc/";

  public AboutWindow()
  {
    InitializeComponent();
    // TODO: This is a workaround, did not get this working in xaml.
    DataContext = new AboutWindowViewModel();
    TxtBxLicense = this.FindControl<TextBox>("TxtBxLicense");
    TxtBxLicense.Text =
      File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Assets", "Doc", "LICENSE"));
    TxtBlkCopyright = this.FindControl<TextBlock>("TxtBlkCopyright");
    TxtBlkCopyright.Text = @"ClubCalc Copyright (C) 2022 Daniel Quack";
    TxtBlkUrl = this.FindControl<TextBlock>("TxtBlkUrl");
    TxtBlkUrl.Text = projectUrl;
    TxtBlkUrl.PointerPressed += Txt_blk_URL_PointerPressed;

#if DEBUG
    this.AttachDevTools();
#endif
  }

  private void Txt_blk_URL_PointerPressed(object? sender, PointerPressedEventArgs e)
  {
    var url = new ProcessStartInfo
    {
      FileName = projectUrl,
      UseShellExecute = true
    };
    Process.Start(url);
  }

  private void InitializeComponent()
  {
    AvaloniaXamlLoader.Load(this);
  }
}