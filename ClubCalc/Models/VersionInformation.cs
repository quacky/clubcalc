namespace ClubCalc.Models;

/// <summary>
///   Provides the formatted version string that can be used within the application.
///   The relevant assembly info is generated with the GitInfo Nuget Package.
/// </summary>
public static class VersionInformation
{
  private static readonly string DirtyPostfix = ThisAssembly.Git.IsDirty ? "-dirty" : string.Empty;

  public static readonly string VersionStringLong =
    $"v{ThisAssembly.Git.SemVer.Major}.{ThisAssembly.Git.SemVer.Minor}.{ThisAssembly.Git.SemVer.Patch}{DirtyPostfix} " +
    $"({ThisAssembly.Git.Commit})";
}