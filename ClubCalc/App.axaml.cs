// Copyright © 2022 Daniel Quack - All Rights Reserved
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY. without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see<http://www.gnu.org/licenses/>.

using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using ClubCalc.DependencyInjection;
using ClubCalc.Helpers;
using ClubCalc.ViewModels;
using ClubCalc.Views;
using Splat;

namespace ClubCalc;

public class App : Application
{
  public static WindowManager WindowManager => WindowManager.Instance;

  public override void Initialize()
  {
//    var dataGridType = typeof(DataGrid); // HACK
    AvaloniaXamlLoader.Load(this);
  }

  public override void OnFrameworkInitializationCompleted()
  {
    if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
    {
      DataContext = GetRequiredService<IMainWindowViewModel>();
      desktop.MainWindow = new MainWindow
      {
        DataContext = DataContext
      };
    }

    base.OnFrameworkInitializationCompleted();
  }

  private static T GetRequiredService<T>()
  {
    return Locator.Current.GetRequiredService<T>();
  }
}