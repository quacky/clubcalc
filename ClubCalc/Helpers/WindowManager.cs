// Copyright © 2022 Daniel Quack - All Rights Reserved
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY. without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see<http://www.gnu.org/licenses/>.

using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClubCalc.Views;
using JetBrains.Annotations;

namespace ClubCalc.Helpers;

public class WindowManager : INotifyPropertyChanged
{
  private static WindowManager? _instance;
  private AboutWindow? _aboutWindow;

  public static WindowManager Instance => _instance ??= new WindowManager();

  public AboutWindow? AboutWindow
  {
    get => _aboutWindow;
    set
    {
      if (Equals(value, _aboutWindow)) return;
      _aboutWindow = value;
      OnPropertyChanged();
    }
  }

  public event PropertyChangedEventHandler? PropertyChanged;

  public void ShowAboutWindow()
  {
    AboutWindow = new AboutWindow();
    AboutWindow.Show();
  }

  [NotifyPropertyChangedInvocator]
  protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
  {
    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
  }
}