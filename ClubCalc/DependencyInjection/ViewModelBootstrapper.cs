// Copyright © 2022 Daniel Quack - All Rights Reserved
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY. without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see<http://www.gnu.org/licenses/>.

using ClubCalc.ViewModels;
using Splat;

namespace ClubCalc.DependencyInjection;

public static class ViewModelBootstrapper
{
  public static void RegisterViewModels(IMutableDependencyResolver services, IReadonlyDependencyResolver resolver)
  {
    services.RegisterLazySingleton<IMainWindowViewModel>(() =>
      new MainWindowViewModel(resolver.GetRequiredService<IMenuViewModel>(),
        resolver.GetRequiredService<ICalculatorViewModel>()));
    services.RegisterLazySingleton<IMenuViewModel>(() => new MenuViewModel());
    services.RegisterLazySingleton<ICalculatorViewModel>(() => new CalculatorViewModel());
  }
}