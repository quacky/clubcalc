# ClubCalc
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://gitlab.com/quacky/clubcalc/-/blob/main/LICENSE)
[![Windows](https://svgshare.com/i/ZhY.svg)](https://www.microsoft.com/de-de/windows?r=1)
[![Linux](https://svgshare.com/i/Zhy.svg)](https://www.linux.org)
[![License: GPL v3](https://badges.frapsoft.com/os/v3/open-source.svg?v=103)](https://schalke04.de)

## Beschreibung
*I am coming soon...*

## Download letzter Prototyp

Der aktuelleste Prototyp kann immer als Artefakt der letzten [Pipeline](https://gitlab.com/quacky/clubcalc/-/pipelines) heruntergeladen werden.